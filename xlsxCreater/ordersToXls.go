package xlsxCreater

import (
	"email-reciever/parser"
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"log"
	"strconv"
)

const (
	defaultSheet = "Sheet1"
)

func CreateXlsxFromOrders (orders []parser.Orders) error {
	f := excelize.NewFile()
	setupHeaders(f)

	var discountPerc float32
	var orderInOneRow string
	for num, order := range orders {
		//задаём смещение
		cellNum := strconv.Itoa(num +2)
		f.SetCellValue(defaultSheet, "A"+cellNum, order.OrderDate)

		f.SetCellValue(defaultSheet, "B"+cellNum, strconv.Itoa(order.OrderID))

		f.SetCellValue(defaultSheet, "C"+cellNum, order.OrdersInfo.Name)

		orderInOneRow = ""
		for num, orderDetails := range order.OrdersDetails {
			orderInOneRow += fmt.Sprintf("%s фасовка %s - %sшт.", orderDetails.Name, orderDetails.Weight, orderDetails.Count)
			if (len(order.OrdersDetails) > 1) && (num != len(order.OrdersDetails) - 1) {
				orderInOneRow += "\n"
			}
		}
		f.SetRowHeight(defaultSheet, num + 2, float64(20 * len(order.OrdersDetails)))
		f.SetCellValue(defaultSheet, "D"+cellNum, orderInOneRow)

		f.SetCellValue(defaultSheet, "E"+cellNum, order.OrderTotal.OrderPrice)

		discount, err1 := strconv.ParseFloat(order.OrderTotal.DiscountSum, 32)
		if err1 != nil {
			log.Println("Error parsing float in discount sum")
		}
		price, err2 := strconv.ParseFloat(order.OrderTotal.OrderPrice, 32)
		if err2 != nil {
			log.Println("Error parsing float in discount sum")
		}
		if err1 == nil && err2 == nil {
			discountPerc = float32(discount / price * 100)
			f.SetCellValue(defaultSheet, "F"+cellNum, fmt.Sprintf("%.2f", discountPerc))
		} else {
			f.SetCellValue(defaultSheet, "F"+cellNum, "Скидка %")
		}

		f.SetCellValue(defaultSheet, "G"+cellNum, order.OrderTotal.DiscountSum)

		f.SetCellValue(defaultSheet, "H"+cellNum, order.OrderTotal.TotalWithDiscount)

		f.SetCellValue(defaultSheet, "I"+cellNum, order.OrdersInfo.Comment)

		f.SetCellValue(defaultSheet, "J"+cellNum, order.OrdersInfo.DeliveryDate)
	}

	// Save spreadsheet by the given path.
	if err := f.SaveAs("Book1.xlsx"); err != nil {
		return err
	}

	return nil
}

func setupHeaders (f *excelize.File) {
	// Set value of a cell.
	f.SetRowHeight(defaultSheet, 1, 35.0)


	f.SetColWidth(defaultSheet, "A", "A", 12.0)
	f.SetCellValue(defaultSheet, "A1", "Дата заказа")

	f.SetColWidth(defaultSheet, "B", "B", 7.0)
	f.SetCellValue(defaultSheet, "B1", "№ Заказа")

	f.SetColWidth(defaultSheet, "C", "C", 30.0)
	f.SetCellValue(defaultSheet, "C1", "ФИО покупателя")

	f.SetColWidth(defaultSheet, "D", "D", 50.0)
	f.SetCellValue(defaultSheet, "D1", "Наименование продукта/продуктов")

	f.SetColWidth(defaultSheet, "E", "E", 10.0)
	f.SetCellValue(defaultSheet, "E1", "Общая стоимость заказа (без учета скидки)")

	f.SetColWidth(defaultSheet, "F", "F", 10.0)
	f.SetCellValue(defaultSheet, "F1", "Скидка %")

	f.SetColWidth(defaultSheet, "G", "G", 10.0)
	f.SetCellValue(defaultSheet, "G1", "Размер скидки")

	f.SetColWidth(defaultSheet, "H", "H", 10.0)
	f.SetCellValue(defaultSheet, "H1", "Общая стоимость заказа (с учетом скидки)")

	f.SetColWidth(defaultSheet, "I", "I", 35.0)
	f.SetCellValue(defaultSheet, "I1", "Комментарии")

	f.SetColWidth(defaultSheet, "J", "J", 12.0)
	f.SetCellValue(defaultSheet, "J1", "Доставка")
}
