package parser

import (
	"fmt"
	"golang.org/x/net/html"
	"strings"
)

type OrderCustomerData struct {
	Name                string
	Tel                 string
	Comment             string
	City                string
	Address             string
	DeliveryDate        string
	DesiredDeliveryDate string
	Email               string
}

type OrderDetails struct {
	Name       string
	Weight     string
	PiecePrice string
	Count      string
	TotalPrice string
}

type OrderTotalDetails struct {
	OrderPrice        string
	DiscountSum       string
	TotalWithDiscount string
}

type Orders struct {
	OrderDate     string
	OrderID       int
	OrdersInfo    OrderCustomerData
	OrdersDetails []OrderDetails
	OrderTotal    OrderTotalDetails
}

func GetTableDataFromMessage(message string) (Orders, error) {
	message = strings.ReplaceAll(message, "></td>", ">-</td>")
	z := html.NewTokenizer(strings.NewReader(message))
	content := []string{}

	// While have not hit the </html> tag
	for z.Token().Data != "html" {
		tt := z.Next()
		if tt == html.StartTagToken {
			t := z.Token()
			if t.Data == "td" {
				inner := z.Next()
				if inner == html.TextToken {
					text := (string)(z.Text())
					t := strings.TrimSpace(text)
					content = append(content, t)
					if strings.Contains(t, "E-mail получателя:") {
						inner = z.Next()
						inner = z.Next()
						inner = z.Next()
						inner = z.Next()
						text = (string)(z.Text())
						text = strings.TrimSpace(text)
						content = append(content, text)
					}
				}
			}
		}
	}

	//Обязательно должно быть не менее 12 полей. 8 до таблицы заказа и 4 после
	//На всякий случай ставим 11
	if len(content) < 11 {
		return Orders{}, fmt.Errorf("Некорректный формат письма ")
	}

	//формируем таблицы
	table1 := content[:16]
	table3 := content[len(content)-4:]
	table2 := content[17 : len(content)-4]

	orderData := OrderCustomerData{
		Name:                table1[1],
		Tel:                 table1[3],
		Comment:             table1[5],
		City:                table1[7],
		Address:             table1[9],
		DeliveryDate:        table1[11],
		DesiredDeliveryDate: table1[13],
		Email:               table1[15],
	}
	// Костыль для нового формата письма
	if strings.Contains(table2[len(table2)-1], "Сумма заказа") {
		x := []string{table2[len(table2)-1]}
		table2 = table2[:len(table2)-1]
		table3 = append(x, table3...)
	}

	price := strings.Split(table3[0], " ")
	discSum := strings.Split(table3[1], " ")

	// Костыль для нового формата письма
	ind := 2
	if strings.Contains(table3[ind], "Процент скидки") {
		ind = 3
	}
	totalWithDisc := strings.Split(table3[ind], " ")

	orderTotal := OrderTotalDetails{
		OrderPrice:        strings.ReplaceAll(price[len(price)-2], ",", "."),
		DiscountSum:       strings.ReplaceAll(discSum[len(discSum)-2], ",", "."),
		TotalWithDiscount: strings.ReplaceAll(totalWithDisc[len(totalWithDisc)-2], ",", "."),
	}

	ordersDetail := Orders{
		OrdersInfo: orderData,
		OrderTotal: orderTotal,
	}
	for i := 0; i < len(table2); i = i + 5 {
		order := OrderDetails{
			Name:       table2[i],
			Weight:     table2[i+1],
			PiecePrice: table2[i+2],
			Count:      table2[i+3],
			TotalPrice: table2[i+4],
		}
		ordersDetail.OrdersDetails = append(ordersDetail.OrdersDetails, order)
	}

	return ordersDetail, nil
}
