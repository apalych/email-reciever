package main

import (
	"email-reciever/emailController"
	"email-reciever/parser"
	"email-reciever/xlsxCreater"
	"fmt"
	"log"
	"strconv"
	"strings"
)

func main() {
	messages, err := emailController.GetAllInboxMails()
	if err != nil {
		log.Fatal(err)
	}
	var totalOrders  []parser.Orders

	for _, msg := range messages {
		//Дополнительно фильтруем письма по признаку
		if strings.Contains(msg.MailBody, "Для подтверждения заказа пройдите по") == false {
			continue
		}
		if strings.Contains(msg.MailBody, "<html") == false {
			msg.MailBody = "<html>" + msg.MailBody + "</html>"
		}
		orderContent, err := parser.GetTableDataFromMessage(msg.MailBody)
		if err != nil {
			log.Println(err.Error())
			continue
		}
		subj := strings.Split(msg.Subject, " ")
		orderContent.OrderID, err = strconv.Atoi(subj[len(subj)-1])
		if err != nil {
			log.Println(err.Error())
			continue
		}
		orderContent.OrderDate = msg.Date
		totalOrders = append(totalOrders, orderContent)
	}

	fmt.Println("Всего заказов будет обработано: ", len(totalOrders))
	err = xlsxCreater.CreateXlsxFromOrders(totalOrders)
	if err != nil {
		log.Fatal(err.Error())
	}
}