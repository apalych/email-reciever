package emailController

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"

	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message/mail"
)

type Mail struct {
	Date     string
	Subject  string
	MailBody string
}

//Необходимо задать свои значения!
const (
	imapAddress = ""
	login = ""
	password = ""
)
func setupConnection() (*client.Client, error) {
	log.Println("Connecting to server...")

	// Connect to server
	c, err := client.DialTLS(imapAddress, nil)
	if err != nil {
		return nil, err
	}
	log.Println("Connected")

	// Login
	if err := c.Login(login, password); err != nil {
		return nil, err
	}
	log.Println("Logged in")

	return c, err
}

func GetAllInboxMails() ([]Mail, error) {
	mailClient, err := setupConnection()
	if err != nil {
		log.Fatal(err)
	}
	// Don't forget to logout
	defer mailClient.Logout()

	// Select INBOX
	mbox, err := mailClient.Select("INBOX", false)
	if err != nil {
		log.Fatal(err)
	}

	// Get the last message
	if mbox.Messages == 0 {
		log.Fatal("No message in mailbox")
	}
	fmt.Println("Количество писем в ящике: ", mbox.Messages)

	seqSet := new(imap.SeqSet)
	seqSet.AddRange(1, mbox.Messages)

	// Get the whole message body
	var section imap.BodySectionName
	items := []imap.FetchItem{section.FetchItem()}

	messages := make(chan *imap.Message, 10)
	done := make(chan error, 1)
	go func() {
		done <- mailClient.Fetch(seqSet, items, messages)
	}()

	var mails []Mail
	for msg := range messages {
		oneMail := Mail{}
		r := msg.GetBody(&section)
		if r == nil {
			return nil, fmt.Errorf("Server didn't returned message body ")
		}

		// Create a new mail reader
		mr, err := mail.CreateReader(r)
		if err != nil {
			return nil, err
		}

		// Print some info about the message
		header := mr.Header
		if date, err := header.Date(); err == nil {
			oneMail.Date = date.Format("02.01.2006")
		}
		if subject, err := header.Subject(); err == nil {
			oneMail.Subject = subject
		}

		// Process each message's part
		for {
			p, err := mr.NextPart()
			if err == io.EOF {
				break
			} else if err != nil {
				return nil, err
			}

			switch h := p.Header.(type) {
			case *mail.InlineHeader:
				// This is the message's text (can be plain-text or HTML)
				b, _ := ioutil.ReadAll(p.Body)
				oneMail.MailBody = string(b)
				mails = append(mails, oneMail)
			case *mail.AttachmentHeader:
				// This is an attachment
				_, _ = h.Filename()
			}
		}
	}

	return mails, nil
}
